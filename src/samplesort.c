/*
 * samplesort.c
 *
 *  Created on: 15 Jan 2015
 *      Author: Martin Mautner
 */

#include "samplesort.h"

size_t rank(elem x, size_t i, const pivot* p, size_t n)
{
	// Bounds
	size_t l = 0;
	size_t u = n - 1;

	// Search
	while (l <= u)
	{
		// Get middle (avoid overflow)
		size_t j = l + ((u - l) / 2);

		if (p[j].val > x)
		{
			if (j == 0 || p[j - 1].val < x)
			{
				return j;	// Found it, no need to check index
			}
			else
			{
				u = j - 1;	// p[j - 1] is not smaller, search in lower half
			}
		}
		else if (p[j].val < x)
		{
			l = j + 1; 	// p[j] is smaller, search in upper half
		}
		else
		{
			if (p[j].idx > i)
			{
				// Continue in lower half if p[j - 1] is still equal to x
				// but the indices still don't add up
				if (j > 0 && p[j - 1].idx >= i && p[j - 1].val == p[j].val)
				{
					u = j - 1;
					continue;
				}
			}
			else if (p[j].idx < i)
			{
				// Search in upper half
				if (j == n - 1 || p[j + 1].val == x)
				{
					l = j + 1;
					continue;
				}
			}

			// At this point the element is equal or we have reached the lower end of the sequence
			// of pivots equal to the element.
			return j;
		}
	}

	// We didn't find anything, thus all elements are either smaller or bigger.
	// The former case is already checked within the loop (because of underflow) -> rank = n
	return n;
}

double get_bucket_expansion(const size_t* bucket_size, size_t p, size_t n)
{
	size_t max = bucket_size[0];

	for (size_t i = 0; i < p; i++)
	{
		if (bucket_size[i] > max)
		{
			max = bucket_size[i];
		}
	}

	return (double) max / ((double) n / (double) p);
}

int pivot_cmp(const void* a, const void* b)
{
	const pivot* x = (const pivot*) a;
	const pivot* y = (const pivot*) b;

	if (x->val < y->val)
	{
		return -1;
	}
	else if (x->val > y->val)
	{
		return 1;
	}
	else
	{
		return (x->idx < y->idx) ? -1 : (x->idx > y->idx);
	}
}
