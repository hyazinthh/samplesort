/*
 * memory.c
 *
 *  Created on: 5 Oct 2015
 *      Author: Martin Mautner
 */

#include "memory.h"

/* === Declarations === */

struct mem_block
{
	void* ptr;
	struct mem_block* next;
};

typedef struct mem_block mem_block;


/* === Global variables === */

mem_block* memory = NULL;


/* === Implementations === */

void* mem_alloc(size_t size)
{
	void* ptr = malloc(size);
	mem_block* blk = (mem_block*) malloc(sizeof(mem_block));

	if (ptr == NULL || blk == NULL)
	{
		free(ptr);
		free(blk);
		bail_out(EXIT_FAILURE, "failed to allocate memory of %zu bytes", size);
	}

	blk->ptr = ptr;
	blk->next = memory;
	memory = blk;

	return ptr;
}

void mem_free()
{
	mem_block* it = memory;

	while (it != NULL)
	{
		mem_block* next = it->next;

		free(it->ptr);
		free(it);

		it = next;
	}

	memory = NULL;
}
