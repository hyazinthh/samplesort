/*
 * samplesort_op.c
 *
 *  Created on: 10 Jun 2015
 *      Author: Martin Mautner
 */

#include <omp.h>
#include "samplesort.h"

//#define USE_PARALLEL_COUNTING
//#define USE_PARALLEL_SAMPLING
//#define USE_RANDOM_SAMPLING

/* === Global variables === */

/**
 * Thread count
 */
size_t p;

/**
 * Oversampling ratio
 */
size_t s;

/**
 * Overpartitioning ratio
 */
size_t k;

/**
 * Auxiliary variables that are used within functions.
 */
size_t* bucket_size;
size_t* thread_load;

/* === Declarations === */

/**
 * Parses the algorithm options and invokes bail_out() on failure.
 *
 * @param[in] ds the dataset to be sorted.
 * @param[in] options the passed options.
 * @param[in] option_count the number of options passed.
 */
void parse_options(dataset* ds, char** options, size_t option_count);

/**
 * Compares two buckets by size and returns the result.
 *
 * @param[in] a pointer to first number
 * @param[in] b pointer to second number
 * @return the result of the comparison
 */
int bucket_cmp(const void* a, const void* b);

/**
 * Downheaps the element at index @p i of the given heap
 * @p heap.
 *
 * @param heap the heap to modify.
 * @param i the element to downheap.
 */
void downheap(size_t* heap, size_t i);


/* === Implementations === */

void parse_options(dataset* ds, char** options, size_t option_count)
{
	// Parse options
	if (option_count != 3 || !str2size_t(options[0], &p)
						  || !str2size_t(options[1], &s)
						  || !str2size_t(options[2], &k))
	{
		bail_out(EXIT_FAILURE, get_usage_message(), command);
	}

	// Check if 1 < p <= min(n, max_threads)
	if (p < 2 || p > ds->length || p > (size_t) omp_get_max_threads())
	{
		bail_out(EXIT_FAILURE, "1 < p <= min(n, %d) must be true", omp_get_max_threads());
	}

	// Check if s > 0
	if (s < 1)
	{
		bail_out(EXIT_FAILURE, "s must be greater than zero");
	}

	// Check if k > 0
	if (k < 1)
	{
		bail_out(EXIT_FAILURE, "k must be greater than zero");
	}

	// Check if k * s * p <= n
	if (k * s * p > ds->length)
	{
		bail_out(EXIT_FAILURE, "k * s * p <= n must be true");
	}
}

benchmark* sort(dataset* ds, char** options, size_t option_count)
{
	// Parse options
	parse_options(ds, options, option_count);

	// Shared variables
	pivot* pvt = mem_array(pivot, k * p - 1);
	pivot* sample = mem_array(pivot, k * s * p);

	thread_load = mem_array(size_t, p);
	size_t* thread_heap = mem_array(size_t, p);

	bucket_size = mem_array(size_t, k * p);
	size_t* bucket_offset = mem_array(size_t, k * p);
	size_t* bucket_order = mem_array(size_t, k * p);
	size_t* bucket_assignment = mem_array(size_t, k * p);

	size_t (*count)[k * p] = mem_array_2d(size_t, p, k * p);	// count[p][k * p]
	size_t (*offset)[k * p] = mem_array_2d(size_t, p, k * p);	// offset[p][k * p]

	// Buffer
	elem* buffer;

	if ((buffer = ARRAY(elem, ds->length)) == NULL)
	{
		bail_out(EXIT_FAILURE, "failed to allocate memory for buffer");
	}

	// Benchmark
	benchmark* bm;

	if ((bm = benchmark_create(5)) == NULL)
	{
		bail_out(EXIT_FAILURE, "failed to allocate memory for benchmark");
	}

	reading local_time[p];
	reading dist_time[p];

	// Sort
	#pragma omp parallel num_threads(p)
	{
			size_t next[k * p];
			size_t local_count[k * p];

			// Rank
			size_t r = (size_t) omp_get_thread_num();

			// Start timer
			benchmark_time_start(&local_time[r], "Runtime (ms)", TU_MILLISECONDS);

			// Clear buckets
			offset[0][r] = 0;

			for (size_t i = 0; i < k; i++)
			{
				bucket_size[r * k + i] = 0;
				bucket_offset[r * k + i] = 0;
				bucket_order[r * k + i] = r * k + i;
			}

			for (size_t i = 0; i < k * p; i++)
			{
				next[i] = 0;
				local_count[i] = 0;
			}

			thread_load[r] = 0;
			thread_heap[r] = r;

			// Compute data segment
			size_t st, end;
			get_segment(r, ds->length, p, &st, &end);

#ifdef USE_PARALLEL_SAMPLING

			for (size_t i = 0; i < k * s; i++)
			{
				sample[r * k * s + i].idx = st + get_segment_end(i, end - st, k * s) - 1;
				sample[r * k * s + i].val = ds->data[sample[r * k * s + i].idx];
			}

	#pragma omp barrier

			#pragma omp single
			{
#else
			#pragma omp single
			{
	#ifdef USE_RANDOM_SAMPLING

				// Get sample
				for (size_t i = 0; i < p * k * s - 1; i++)
				{
					// Get extents of segment
					size_t st_t, end_t;
					get_segment(i, ds->length, p * k * s - 1, &st_t, &end_t);

					// Take random element within segment
					sample[i].idx = st_t + ((size_t) rand() % (end_t - st_t));
					sample[i].val = ds->data[sample[i].idx];
				}
	#else

				// Get sample
				for (size_t i = 0; i < k * s * p - 1; i++)
				{
					sample[i].idx = get_segment_end(i, ds->length, k * s * p) - 1;
					sample[i].val = ds->data[sample[i].idx];
				}

	#endif
#endif

				// Sort sample
				benchmark_time_start(&bm->readings[1], "Sample sorting (ms)", TU_MILLISECONDS);

				qsort(sample, k * s * p - 1, sizeof(pivot), pivot_cmp);

				benchmark_time_end(&bm->readings[1], TU_MILLISECONDS);

				// Select splitters
				for (size_t i = 0; i < k * p - 1; i++)
				{
					pvt[i] = sample[(i + 1) * s - 1];
				}
			}

			for (size_t i = st; i < end; i++)
			{
				local_count[rank(ds->data[i], i, pvt, k * p - 1)]++;
			}

			for (size_t i = 0; i < k * p; i++)
			{
				count[r][i] = local_count[i];	// Make sure false sharing has minimal effects
			}

		#pragma omp barrier

#ifdef USE_PARALLEL_COUNTING

			for (size_t i = 0; i < k; i++)
			{
				// Compute prefix sums over count array
				prefix_sums(&count[0][r * k + i], &offset[0][r * k + i], k * p, p);

				// Compute final bucket size
				bucket_size[r * k + i] = offset[p - 1][r * k + i] + count[p - 1][r * k + i];
			}

		#pragma omp barrier

			// Compute prefix sums
			#pragma omp single
			{
				prefix_sums(bucket_size, bucket_offset, 1, k * p);
			}
#else
			#pragma omp single
			{
				// Compute prefix sums over count array
				for (size_t i = 0; i < k * p; i++)
				{
					prefix_sums(&count[0][i], &offset[0][i], k * p, p);
				}

				// Compute final bucket size
				for (size_t i = 0; i < k * p; i++)
				{
					bucket_size[i] = offset[p - 1][i] + count[p - 1][i];
				}

				// Prefix sums for bucket offsets
				prefix_sums(bucket_size, bucket_offset, 1, k * p);
			}
#endif

			// Copy
			benchmark_time_start(&dist_time[r], "Distribution (ms)", TU_MILLISECONDS);

			for (size_t i = st; i < end; i++)
			{
				size_t b = rank(ds->data[i], i, pvt, k * p - 1);
				buffer[bucket_offset[b] + offset[r][b] + next[b]++] = ds->data[i];
			}

			benchmark_time_end(&dist_time[r], TU_MILLISECONDS);

		#pragma omp barrier

			#pragma omp single
			{
				// Sort buckets by size
				qsort(bucket_order, k * p, sizeof(size_t), bucket_cmp);

				// Assign buckets
				for (size_t i = 0; i < k * p; i++)
				{
					// Get thread with minimum load
					size_t t = thread_heap[0];

					// Assign bucket
					bucket_assignment[bucket_order[i]] = t;
					thread_load[t] += bucket_size[bucket_order[i]];

					// Update heap
					downheap(thread_heap, 0);
				}
			}

			// Sort assigned buckets
			for (size_t i = 0; i < k * p; i++)
			{
				if (bucket_assignment[i] == r)
				{
					qsort(&buffer[bucket_offset[i]], bucket_size[i], sizeof(elem), real_cmp);
				}
			}

			// End time
			benchmark_time_end(&local_time[r], TU_MILLISECONDS);
	}

	// Total time
	bm->readings[0] = get_max_reading(local_time, p);

	// Distribution
	bm->readings[2] = get_max_reading(dist_time, p);

	// Bucket expansion
	bm->readings[3].name = "Bucket expansion";
	bm->readings[3].value = get_bucket_expansion(bucket_size, k * p, ds->length);

	// Load expansion
	bm->readings[4].name = "Load expansion";
	bm->readings[4].value = get_bucket_expansion(thread_load, p, ds->length);

	// Switch buffers
	free(ds->data);
	ds->data = buffer;

	// Cleanup
	mem_free();

	return bm;
}

int bucket_cmp(const void* a, const void* b)
{
	const size_t* x = (const size_t*) a;
	const size_t* y = (const size_t*) b;

	return (bucket_size[*x] > bucket_size[*y]) ? -1 : (bucket_size[*x] < bucket_size[*y]);
}

void downheap(size_t* heap, size_t i)
{
	size_t l = 2 * i;
	size_t r = l + 1;

	if (l < p)
	{
		size_t t;

		if (r < p)
		{
			t = (thread_load[heap[l]] < thread_load[heap[r]]) ? l : r;
		}
		else
		{
			t = l;
		}

		if (thread_load[heap[t]] < thread_load[heap[i]])
		{
			size_t tmp = heap[i];
			heap[i] = heap[t];
			heap[t] = tmp;
			downheap(heap, t);
		}
	}
}

const char* get_usage_message()
{
	return "Usage: %s [-p] [ -g | -n percent] input thread_count oversampling_ratio overpartitioning_ratio";
}
