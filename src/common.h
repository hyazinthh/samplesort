/*
 * common.h
 *
 *  Created on: 8 Jan 2015
 *      Author: Martin Mautner
 */

#ifndef SRC_COMMON_H_
#define SRC_COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>

/* === Macros === */

#define SAFE_FREE(x) free(x); (x) = NULL

// Fancy debug from http://stackoverflow.com/questions/1644868/c-define-macro-for-debug-printing
#ifdef _DEBUG
	#define DEBUG(...) do { fprintf(stderr, "%s:%d:%s(): ", __FILE__, __LINE__, __func__); 		   \
							fprintf(stderr, __VA_ARGS__);										   \
							fprintf(stderr, "\n"); } while (0)

	#define DEBUG_OMP(...)	_Pragma("omp critical") { DEBUG(__VA_ARGS__); }
#else
	#define DEBUG(...)
	#define DEBUG_OMP(...)
#endif

// Safe min / max (http://stackoverflow.com/questions/3437404/min-and-max-in-c)
#define MIN(a, b) __extension__ ({ __typeof__ (a) _a = (a); \
								   __typeof__ (b) _b = (b); \
								   _a < _b ? _a : _b; })

#define MAX(a, b) __extension__ ({ __typeof__ (a) _a = (a); \
								   __typeof__ (b) _b = (b); \
								   _a > _b ? _a : _b; })


#define ARRAY(t, n)	(t*) malloc(sizeof(t) * (n))

#define UNUSED(x) (void)(x)


#define ELEM_FMT "%f"

// Color for output
//#define COLOR_OUTPUT

#ifdef COLOR_OUTPUT
	#define C_RED     "\x1b[31;1m"
	#define C_GREEN   "\x1b[32;1m"
	#define C_YELLOW  "\x1b[33;1m"
	#define C_MAGENTA "\x1b[35;1m"
	#define C_CYAN    "\x1b[36;1m"
	#define C_RESET   "\x1b[0;0m"
#else
	#define C_RED
	#define C_GREEN
	#define C_YELLOW
	#define C_MAGENTA
	#define C_CYAN
	#define C_RESET
#endif

/* === Declarations === */

typedef unsigned int uint;

/**
 * Basic datatype.
 */
typedef float elem;

/**
 * Prints the message of the given error number.
 * If @p error is zero, the invocation of the function has not effect.
 *
 * @param[in] stream the stream to print to.
 * @param[in] error the error to print.
 */
void print_error(FILE* stream, int error);

/**
 * Terminates program on program error.
 *
 * @param[in] eval the exit code.
 * @param[in] fmt the format string of the exit message.
 */
void bail_out(int eval, const char* fmt, ...);

/**
 * Frees resources used by the program.
 */
void free_resources(void);

/**
 * Converts the given format to a string.
 * The memory for the string is automatically allocated and has to be
 * freed by the client.
 *
 * @param[in] fmt The format string.
 * @return The expanded string on success, otherwise NULL.
 */
char* to_string(const char* fmt, ...);

/**
 * Compares two real numbers and returns the result.
 *
 * @param[in] a pointer to first number
 * @param[in] b pointer to second number
 * @return the result of the comparison
 */
int real_cmp(const void* a, const void* b);

/**
 * Converts a string to a double.
 *
 * @param[in] str the string to be parsed.
 * @param[out] rs the result of the conversion.
 * @return true if successful, false otherwise.
 */
bool str2double(const char* str, double* rs);

/**
 * Converts a string to a size_t.
 *
 * @param[in] str the string to be parsed.
 * @param[out] rs the result of the conversion.
 * @return true if successful, false otherwise.
 */
bool str2size_t(const char* str, size_t* rs);


#endif /* SRC_COMMON_H_ */
