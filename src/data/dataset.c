/*
 * dataset.c
 *
 *  Created on: 8 Jan 2015
 *      Author: Martin Mautner
 */

#include "dataset.h"

dataset* dataset_create(dataset* ds)
{
	// Check parameters
	if (ds == NULL || ds->length == 0)
	{
		errno = EINVAL;
		return NULL;
	}

	// Allocate
	ds->data = (elem*) malloc(ds->length * sizeof(elem));

	if (ds->data == NULL)
	{
		return NULL;
	}
	else
	{
		return ds;
	}
}

void dataset_free(dataset* ds)
{
	if (ds != NULL)
	{
		free(ds->data);
		ds->data = NULL;
		ds->length = 0;
	}
}

void dataset_copy(dataset* dest, const dataset* src)
{
	memcpy(dest->data, src->data, src->length * sizeof(elem));
}
