/*
 * benchmark.h
 *
 *  Created on: 13 Apr 2015
 *      Author: Martin Mautner
 */

#ifndef SRC_BENCHMARK_H_
#define SRC_BENCHMARK_H_

#include "common.h"

#ifndef CLOCK_MONOTONIC_RAW
#define CLOCK_MONOTONIC_RAW CLOCK_MONOTONIC
#endif

/**
 * Typedef for timespec
 */
typedef struct timespec timespec;

/**
 * Enumeration for time units
 */
typedef enum
{
	TU_NANOSECONDS  = 1,
	TU_MICROSECONDS = 1000,
	TU_MILLISECONDS = 1000 * 1000,
	TU_SECONDS	  	= 1000 * 1000 * 1000,
} time_unit;

/**
 * Struct for a single reading.
 */
typedef struct
{
	const char* name;
	double value;

} reading;

/**
 * Struct for a benchmark.
 * A benchmark consists of possibly multiply readings, which are
 * name - value pairs.
 */
typedef struct
{
	size_t reading_count;
	reading* readings;

} benchmark;

/**
 * Creates a benchmark for @p n readings.
 *
 * @param[in] n the number of readings that will be contained.
 * @return the newly created benchmark struct, or NULL on failure
 */
benchmark* benchmark_create(size_t n);

/**
 * Starts a time reading.
 *
 * @param[in] r the reading to be initialized.
 * @param[in] name the name of the reading.
 * @param[in] u the time unit of the reading.
 */
void benchmark_time_start(reading* r, const char* name, time_unit u);

/**
 * Finishes a time reading.
 *
 * @param[in] r the reading to be finished.
 * @param[in] u the time unit of the reading.
 */
void benchmark_time_end(reading* r, time_unit u);

/**
 * Frees the given benchmark.
 * @param[in] bm the benchmark to free.
 */
void benchmark_free(benchmark* bm);

/**
 * Finds the reading with the highest value.
 *
 * @param r an array of @p n readings.
 * @param n the number of readings.
 * @return the reading with the highest value.
 */
reading get_max_reading(reading* r, size_t n);

#endif /* SRC_BENCHMARK_H_ */
