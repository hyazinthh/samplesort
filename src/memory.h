/*
 * memory.h
 *
 *  Created on: 5 Oct 2015
 *      Author: Martin Mautner
 */

#ifndef SRC_MEMORY_H_
#define SRC_MEMORY_H_

#include "common.h"

/* === Macros === */

#define mem_array(t, n)	(t*) mem_alloc(sizeof(t) * (n))

#define mem_array_2d(t, w, h) mem_alloc(sizeof(t) * (w) * (h))


/* === Declarations === */

/**
 * Allocates a block of memory.
 * Calls bail_out() on failure.
 *
 * @param size the size in bytes of the block to allocate.
 */
void* mem_alloc(size_t size);

/**
 * Frees all allocated blocks of memory.
 */
void mem_free();


#endif /* SRC_MEMORY_H_ */
