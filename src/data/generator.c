/*
 * generator.c
 *
 *  Created on: 10 Jan 2015
 *      Author: Martin Mautner
 */

#include <math.h>
#include <limits.h>
#include "generator.h"

/* === Constants === */

#define LOWER_LIMIT ((elem) -50000000)
#define UPPER_LIMIT ((elem)  50000000)
#define SEGMENT_COUNT ((size_t) 512)

/* === Declarations === */

/**
 * @return a random number between 0 and 1.
 */
double rand_p();

/* === Implementations === */

dataset* dataset_generate(dataset* ds)
{
	return dataset_generate_ext(ds, LOWER_LIMIT, UPPER_LIMIT);
}

dataset* dataset_generate_asc(dataset* ds)
{
	// Allocate memory
	if (!dataset_create(ds))
	{
		return NULL;
	}

	// Generate random data
	double step = (UPPER_LIMIT - LOWER_LIMIT) / (double) ds->length;

	for (size_t i = 0; i < ds->length; i++)
	{
		elem l = LOWER_LIMIT + (elem) (step * (double) (i));
		elem u = LOWER_LIMIT + (elem) (step * (double) (i + 1));
		ds->data[i] = (elem) (l + rand_p() * (u - l));
	}

	// Success
	return ds;
}

dataset* dataset_generate_desc(dataset* ds)
{
	// Allocate memory
	if (!dataset_create(ds))
	{
		return NULL;
	}

	// Generate random data
	double step = (UPPER_LIMIT - LOWER_LIMIT) / (double) ds->length;

	for (size_t i = 0; i < ds->length; i++)
	{
		elem l = UPPER_LIMIT - (elem) (step * (double) (i + 1));
		elem u = UPPER_LIMIT - (elem) (step * (double) (i));
		ds->data[i] = (elem) (l + rand_p() * (u - l));
	}

	// Success
	return ds;
}

dataset* dataset_generate_repeating(dataset* ds)
{
	// Allocate memory
	if (!dataset_create(ds))
	{
		return NULL;
	}

	// Calculate the length of the sequence
	size_t seq = MAX((size_t) 1, ds->length / SEGMENT_COUNT);

	// Generate sequence
	for (size_t i = 0; i < seq; i++)
	{
		ds->data[i] = (elem) (LOWER_LIMIT + rand_p() * (UPPER_LIMIT - LOWER_LIMIT));
	}

	// Repeat sequence
	for (size_t i = seq; i < ds->length; i++)
	{
		ds->data[i] = ds->data[i % seq];
	}

	// Success
	return ds;
}

dataset* dataset_generate_ext(dataset* ds, elem min, elem max)
{
	// Allocate memory
	if (!dataset_create(ds))
	{
		return NULL;
	}

	// Generate random data
	for (size_t i = 0; i < ds->length; i++)
	{
		ds->data[i] = (elem) (min + rand_p() * (max - min));
	}

	// Success
	return ds;
}

double rand_p()
{
	return ((double) rand()) / RAND_MAX;
}
