/*
 * samplesort.h
 *
 *  Created on: 6 May 2015
 *      Author: Martin Mautner
 */

#ifndef SRC_SAMPLESORT_H_
#define SRC_SAMPLESORT_H_

#include "sort.h"
#include "memory.h"

/* === Declarations === */

/**
 * Struct for pivot elements.
 */
typedef struct
{
	elem val;
	size_t idx;
} pivot;

/**
 * Computes the appropriate bucket of the element @p x, given
 * an array of pivot elements @p p. The bucket selection depends on the
 * values but also on the indices to support same valued elements.
 *
 * @param[in] x the element.
 * @param[in] i the index of the element.
 * @param[in] p the array of pivot elements.
 * @param[in] n the number of elements in the pivot array,
 * @return the bucket of the element.
 */
size_t rank(elem x, size_t i, const pivot* p, size_t n);

/**
 * Computes the exclusive prefix sums of @p a and stores them in @p b.
 *
 * @param[in] a the array of which the prefix sums are to be computed.
 * @param[in,out] b the array to hold the computed prefix sums.
 * @param[in] stride the stride of @p a and @p b.
 * @param[in] n the number of elements in @p a and @p b.
 */
static inline void prefix_sums(const size_t* a, size_t* b, size_t stride, size_t n);

/**
 * Returns the start and end of the i-th segment, splitting a
 * sequence of length @p n into @p p equal parts.
 *
 * @param[in] i the index of the segment.
 * @param[in] n the length of the overall sequence.
 * @param[in] p the number of segments to create.
 * @param[out] st the start of the segment.
 * @param[out] end the end of the segment.
 */
static inline void get_segment(size_t i, size_t n, size_t p, size_t* st, size_t* end);

/**
 * Returns the end of the i-th segment, splitting a
 * sequence of length @p n into @p p equal parts.
 *
 * @param[in] i the index of the segment.
 * @param[in] n the length of the overall sequence.
 * @param[in] p the number of segments to create.
 */
static inline size_t get_segment_end(size_t i, size_t n, size_t p);

/**
 * Computes the bucket expansion, thus the ratio of the largest
 * bucket size to the ideal bucket size @p n / @p p.
 *
 * @param[in] bucket_size the bucket sizes.
 * @param[in] p the number of buckets.
 * @param[in] n the number of elements to be sorted.
 * @return the bucket expansion.
 */
double get_bucket_expansion(const size_t* bucket_size, size_t p, size_t n);

/**
 * Compares two pivot elements and returns the result.
 *
 * @param[in] a pointer to first element.
 * @param[in] b pointer to second element.
 * @return the result of the comparison.
 */
int pivot_cmp(const void* a, const void* b);


/* === Implementations === */

static inline void prefix_sums(const size_t* a, size_t* b, size_t stride, size_t n)
{
	b[0] = 0;

	for (size_t i = 1; i < n; i++)
	{
		b[i * stride] = a[(i - 1) * stride] + b[(i - 1) * stride];
	}
}

static inline void get_segment(size_t i, size_t n, size_t p, size_t* st, size_t* end)
{
	float div = (float) n / (float) p;
	*st = (size_t) (((float) i) * div);
	*end = (size_t) (((float) i + 1) * div);
}

static inline size_t get_segment_end(size_t i, size_t n, size_t p)
{
	return (size_t) (((float) i + 1) * ((float) n / (float) p));
}


#endif /* SRC_SAMPLESORT_H_ */
