/*
 * sort.h
 *
 *  Created on: 15 Jan 2015
 *      Author: Martin Mautner
 */

#ifndef SRC_SORT_H_
#define SRC_SORT_H_

#include "common.h"
#include "benchmark.h"
#include "data/dataset.h"

/* === Global variables === */

/**
 * The command name.
 */
char* command;

/* === Declarations === */

/**
 * Interface for all sort implementations.
 * Sorts the dataset @p ds and returns various statistics of the algorithm.
 * The returned benchmark struct must be freed by the caller.
 *
 * @param[in] ds the dataset to be sorted
 * @param[in] options an array of options for the algorithm, or NULL.
 * @param[in] option_count the number of options in @p options.
 * @return a benchmark struct containing various statistics.
 */
benchmark* sort(dataset* ds, char** options, size_t option_count);

/**
 * @return the usage message for the sort algorithm.
 */
const char* get_usage_message();


#endif /* SRC_SORT_H_ */
