/*
 * io.h
 *
 *  Created on: 8 Jan 2015
 *      Author: Martin Mautner
 */

#ifndef SRC_DATA_IO_H_
#define SRC_DATA_IO_H_

#include "dataset.h"

/**
 * Reads a dataset from the given file.
 *
 * @param[in] fname the path of the file holding the data.
 * @param[out] ds dataset struct used for output.
 * @param[in] portion specifies the portion of the dataset to read, 0 < portion <= 1
 * @return the struct passed as ouput parameter on success, or NULL if an error occurs.
 */
dataset* dataset_read(const char* fname, dataset* ds, double portion);

/**
 * Writes a dataset to the given files.
 *
 * @param[in] fname the path of the output file.
 * @param[in] ds the dataset to be written.
 * @return true on success, false if an error occurs.
 */
bool dataset_write(const char* fname, const dataset* ds);

/**
 * Prints a dataset to the given stream.
 *
 * @param[in] stream the stream to print to.
 * @param[in] ds the dataset to be printed.
 */
void dataset_print(FILE* stream, const dataset* ds);

/**
 * Prints a dataset to the given stream using the given format specifier
 * for each element.
 *
 * @param[in] stream the stream to print to.
 * @param[in] ds the dataset to be printed.
 * @param[in] fmt the format specifier used to print a single element, or NULL for a default format.
 */
void dataset_print_ext(FILE* stream, const dataset* ds, const char* fmt);

#endif /* SRC_DATA_IO_H_ */
