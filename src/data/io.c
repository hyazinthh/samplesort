/*
 * io.c
 *
 *  Created on: 8 Jan 2015
 *      Author: Martin Mautner
 */

#include "io.h"

/* === Implementations === */

dataset* dataset_read(const char* fname, dataset* ds, double portion)
{
	// Check parameters
	if (fname == NULL || ds == NULL)
	{
		errno = EINVAL;
		return NULL;
	}

	// Open file
	FILE* f = fopen(fname, "rb");
	if (f == NULL)
	{
		return NULL;
	}

	// Get number of elements
	size_t count;
	if (fread(&count, sizeof(count), 1, f) < 1)
	{
		fclose(f);
		return NULL;
	}

	count = (size_t) ((double) count * portion);

	// Allocate buffer
	elem* buffer = (elem*) malloc(count * sizeof(elem));
	if (buffer == NULL)
	{
		fclose(f);
		return NULL;
	}

	// Read data
	if (fread(buffer, sizeof(elem), count, f) < count)
	{
		free(buffer);
		fclose(f);
		return NULL;
	}
	// Success
	ds->data = buffer;
	ds->length = count;
	fclose(f);

	// Return
	return ds;
}

bool dataset_write(const char* fname, const dataset* ds)
{
	// Check parameters
	if (fname == NULL || ds == NULL || ds->data == NULL)
	{
		errno = EINVAL;
		return false;
	}
	// Open file
	FILE* f = fopen(fname, "wb");
	if (f == NULL)
	{
		return false;
	}

	// Write element count
	if (fwrite(&ds->length, sizeof(ds->length), 1, f) < 1)
	{
		fclose(f);
		return false;
	}

	// Write data
	if (fwrite(ds->data, sizeof(elem), ds->length, f) < ds->length)
	{
		fclose(f);
		return false;
	}

	// Success
	fclose(f);
	return true;
}

void dataset_print(FILE* stream, const dataset* ds)
{
	dataset_print_ext(stream, ds, NULL);
}

void dataset_print_ext(FILE* stream, const dataset* ds, const char* fmt)
{
	// Check parameters
	if (stream == NULL || ds == NULL || ds->data == NULL)
	{
		errno = EINVAL;
		return;
	}

	// Print
	fprintf(stream, "[");

	for (size_t i = 0; i < ds->length - 1; i++)
	{

		fprintf(stream, (fmt != NULL) ? fmt : ELEM_FMT, ds->data[i]);
		fprintf(stream, ", ");
	}

	fprintf(stream, (fmt != NULL) ? fmt : ELEM_FMT, ds->data[ds->length - 1]);
	fprintf(stream, "]");
}
