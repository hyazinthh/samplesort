/*
 * dataset.h
 *
 *  Created on: 8 Jan 2015
 *      Author: Martin Mautner
 */

#ifndef SRC_DATA_DATASET_H_
#define SRC_DATA_DATASET_H_

#include "../common.h"

/**
 * Struct for datasets.
 */
typedef struct
{
	/**
	 * The actual data
	 */
	elem* data;

	/**
	 * The number of elements
	 */
	size_t length;

} dataset;

/**
 * Allocates memory for a dataset.
 *
 * @param[in, out] ds the struct used for output, @p ds.length determines the size of the dataset.
 * @return the struct passed as ouput parameter on success, or NULL if an error occurs.
 */
dataset* dataset_create(dataset* ds);

/**
 * Releases the memory allocated by the given dataset.
 * Frees the internal buffer of the dataset and sets its length property to
 * zero. If NULL is passed as parameter, the invocation has no effect.
 *
 * @param ds the dataset to be released, or NULL.
 */
void dataset_free(dataset* ds);

/**
 * Copies the given dataset @p src to @p dest.
 * The memory for @p dest must be allocated by the caller and be large
 * enough to hold the data from @p src.
 *
 * @param dest[in] the destination of the copy process.
 * @param src[in] the dataset to copy.
 */
void dataset_copy(dataset* dest, const dataset* src);

#endif /* SRC_DATA_DATASET_H_ */
