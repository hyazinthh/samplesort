/*
 * samplesort_pd_ex.c
 *
 *  Created on: 27 Apr 2015
 *      Author: Martin Mautner
 */

#include <omp.h>
#include "samplesort.h"

//#define USE_PARALLEL_COUNTING
//#define USE_PARALLEL_SAMPLING
//#define USE_RANDOM_SAMPLING

/* === Global variables === */

/**
 * Thread count
 */
size_t p;

/**
 * Oversampling ratio
 */
size_t s;

/* === Declarations === */

/**
 * Parses the algorithm options and invokes bail_out() on failure.
 *
 * @param[in] ds the dataset to be sorted.
 * @param[in] options the passed options.
 * @param[in] option_count the number of options passed.
 */
void parse_options(dataset* ds, char** options, size_t option_count);

/* === Implementations === */

void parse_options(dataset* ds, char** options, size_t option_count)
{
	// Parse options
	if (option_count != 2 || !str2size_t(options[0], &p) || !str2size_t(options[1], &s))
	{
		bail_out(EXIT_FAILURE, get_usage_message(), command);
	}

	// Check if 1 < p <= min(n, max_threads)
	if (p < 2 || p > ds->length || p > (size_t) omp_get_max_threads())
	{
		bail_out(EXIT_FAILURE, "1 < p <= min(n, %d) must be true", omp_get_max_threads());
	}

	// Check if s > 0
	if (s < 1)
	{
		bail_out(EXIT_FAILURE, "s must be greater than zero");
	}

	// Check if s * p <= n
	if (s * p > ds->length)
	{
		bail_out(EXIT_FAILURE, "s * p <= n must be true");
	}
}

benchmark* sort(dataset* ds, char** options, size_t option_count)
{
	// Parse options
	parse_options(ds, options, option_count);

	// Shared variables
	pivot* pvt = mem_array(pivot, p - 1);
	pivot* sample = mem_array(pivot, s * p);
	size_t* bucket_size = mem_array(size_t, p);
	size_t* bucket_offset = mem_array(size_t, p);
	size_t (*count)[p] = mem_array_2d(size_t, p, p);	// count[r][i] elements will go to bucket i from thread r
	size_t (*offset)[p] = mem_array_2d(size_t, p, p);

	// Output buffer
	elem* buffer;

	if ((buffer = ARRAY(elem, ds->length)) == NULL)
	{
		bail_out(EXIT_FAILURE, "failed to allocate memory for buffer");
	}

	// Benchmark
	benchmark* bm;

	if ((bm = benchmark_create(4)) == NULL)
	{
		bail_out(EXIT_FAILURE, "failed to allocate memory for benchmark");
	}

	reading local_time[p];
	reading dist_time[p];

	// Sort
	#pragma omp parallel num_threads(p)
	{
			size_t next[p];
			size_t local_count[p];

			// Rank
			size_t r = (size_t) omp_get_thread_num();

			// Start timer
			benchmark_time_start(&local_time[r], "Runtime (ms)", TU_MILLISECONDS);

			// Clear buckets
			bucket_size[r] = 0;
			bucket_offset[r] = 0;
			offset[0][r] = 0;

			for (size_t i = 0; i < p; i++)
			{
				next[i] = 0;
				local_count[i] = 0;
			}

			// Compute data segment
			size_t st, end;
			get_segment(r, ds->length, p, &st, &end);

#ifdef USE_PARALLEL_SAMPLING

			// Get sample
			for (size_t i = 0; i < s; i++)
			{
				sample[r * s + i].idx = st + get_segment_end(i, end - st, s) - 1;
				sample[r * s + i].val = ds->data[sample[r * s + i].idx];
			}

	#pragma omp barrier

			#pragma omp single
			{
#else
			#pragma omp single
			{
	#ifdef USE_RANDOM_SAMPLING

				// Get sample
				for (size_t i = 0; i < s * p - 1; i++)
				{
					// Get extents of segment
					size_t st_t, end_t;
					get_segment(i, ds->length, s * p - 1, &st_t, &end_t);

					// Take random element within segment
					sample[i].idx = st_t + ((size_t) rand() % (end_t - st_t));
					sample[i].val = ds->data[sample[i].idx];
				}
	#else
				
				// Get sample
				for (size_t i = 0; i < s * p - 1; i++)
				{
					sample[i].idx = get_segment_end(i, ds->length, s * p) - 1;
					sample[i].val = ds->data[sample[i].idx];
				}

	#endif
#endif
				// Sort sample
				benchmark_time_start(&bm->readings[1], "Sample sorting (ms)", TU_MILLISECONDS);

				qsort(sample, s * p - 1, sizeof(pivot), pivot_cmp);

				benchmark_time_end(&bm->readings[1], TU_MILLISECONDS);

				// Select splitters
				for (size_t i = 0; i < p - 1; i++)
				{
					pvt[i] = sample[(i + 1) * s - 1];
				}
			}

			for (size_t i = st; i < end; i++)
			{
				local_count[rank(ds->data[i], i, pvt, p - 1)]++;
			}

			for (size_t i = 0; i < p; i++)
			{
				count[r][i] = local_count[i];	// Make sure false sharing has minimal effects
			}

		#pragma omp barrier

#ifdef USE_PARALLEL_COUNTING

			// Compute prefix sums over count array
			prefix_sums(&count[0][r], &offset[0][r], p, p);

			// Compute final bucket size
			bucket_size[r] = offset[p - 1][r] + count[p - 1][r];

		#pragma omp barrier

			// Compute prefix sums
			#pragma omp single
			{
				prefix_sums(bucket_size, bucket_offset, 1, p);
			}
#else
			#pragma omp single
			{
				// Compute prefix sums over count array
				for (size_t i = 0; i < p; i++)
				{
					prefix_sums(&count[0][i], &offset[0][i], p, p);
				}

				// Compute final bucket size
				for (size_t i = 0; i < p; i++)
				{
					bucket_size[i] = offset[p - 1][i] + count[p - 1][i];
				}

				// Prefix sums for bucket offsets
				prefix_sums(bucket_size, bucket_offset, 1, p);
			}
#endif

			// Copy
			benchmark_time_start(&dist_time[r], "Distribution (ms)", TU_MILLISECONDS);

			for (size_t i = st; i < end; i++)
			{
				size_t b = rank(ds->data[i], i, pvt, p - 1);
				buffer[bucket_offset[b] + offset[r][b] + next[b]++] = ds->data[i];
			}

			benchmark_time_end(&dist_time[r], TU_MILLISECONDS);

		#pragma omp barrier

			// Sort bucket
			qsort(&buffer[bucket_offset[r]], bucket_size[r], sizeof(elem), real_cmp);

			// End time
			benchmark_time_end(&local_time[r], TU_MILLISECONDS);
	}

	// Total time
	bm->readings[0] = get_max_reading(local_time, p);

	// Distribution
	bm->readings[2] = get_max_reading(dist_time, p);

	// Bucket expansion
	bm->readings[3].name = "Bucket expansion";
	bm->readings[3].value = get_bucket_expansion(bucket_size, p, ds->length);

	// Switch buffers
	free(ds->data);
	ds->data = buffer;

	// Cleanup
	mem_free();

	return bm;
}

const char* get_usage_message()
{
	return "Usage: %s [-p] [ -g | -n percent] input thread_count oversampling_ratio";
}
