/*
 * common.c
 *
 *  Created on: 8 Jan 2015
 *      Author: Martin Mautner
 */

#include "common.h"

/* === Constants === */

/**
 * Buffer size for strerror_r()
 */
#define BUFFER_SIZE 256

/* === Implementations === */

void print_error(FILE* stream, int error)
{
	// Check parameters
	if (stream == NULL || error == 0)
	{
		return;
	}

	// Print
	char buf[BUFFER_SIZE];
	if (strerror_r(error, buf, BUFFER_SIZE) == 0)
	{
		fprintf(stream, buf);
	}
}

void bail_out(int eval, const char* fmt, ...)
{
	// Save errno
	int errsv = errno;

	// Print user-provided message
	va_list ap;

	if (fmt != NULL)
	{
		va_start(ap, fmt);
		vfprintf(stderr, fmt, ap);
		va_end(ap);
	}

	// Print errno
	if (errsv != 0)
	{
		fprintf(stderr, ": ");
		print_error(stderr, errsv);
	}

	fprintf(stderr, "\n");

	// Free
	free_resources();

	// Exit
	exit(eval);
}

char* to_string(const char* fmt, ...)
{
	if (fmt == NULL)
	{
		return NULL;
	}

	va_list ap, ap2;

	va_start(ap, fmt);
	va_copy(ap2, ap);

	size_t n = (size_t) vsnprintf(NULL, 0, fmt, ap) + 1;

	char* r = malloc(n);
	if (r == NULL)
	{
		return NULL;
	}

	vsnprintf(r, n, fmt, ap2);

	va_end(ap2);
	va_end(ap);

	return r;
}

int real_cmp(const void* a, const void* b)
{
	const elem* x = (const elem*) a;
	const elem* y = (const elem*) b;

	return (*x < *y) ? -1 : (*x > *y);
}

bool str2double(const char* str, double* rs)
{
	// Convert
	errno = 0;
	char* endptr;
	double val = strtod(str, &endptr);

	// Check
	if (errno != 0 || endptr == str || val < 0)
	{
		return false;
	}

	// Return
	*rs = val;
	return true;
}

bool str2size_t(const char* str, size_t* rs)
{
	// Convert
	errno = 0;
	char* endptr;
	long val = strtol(str, &endptr, 10);

	// Check
	if (errno != 0 || endptr == str || val < 0 || (unsigned long) val > SIZE_MAX)
	{
		return false;
	}

	// Return
	*rs = (size_t) val;
	return true;
}


