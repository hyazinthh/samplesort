/*
 * sort.c
 */

#include "sort.h"

benchmark* sort(dataset* ds, char** options, size_t option_count)
{
	UNUSED(options);
	UNUSED(option_count);

	// Benchmark
	benchmark* bm;

	if ((bm = benchmark_create(1)) == NULL)
	{
		bail_out(EXIT_FAILURE, "failed to allocate memory for benchmark");
	}

	// Sort
	benchmark_time_start(bm->readings, "Runtime (ms)", TU_MILLISECONDS);

	qsort(ds->data, ds->length, sizeof(elem), real_cmp);

	benchmark_time_end(bm->readings, TU_MILLISECONDS);

	// Return benchmark
	return bm;
}

const char* get_usage_message()
{
	return "Usage: %s [-p] [-g | -n percent] input";
}
