/*
 * benchmark.c
 *
 *  Created on: 13 Apr 2015
 *      Author: Martin Mautner
 */

#include "benchmark.h"

/* === Macros === */

#define TTD(t, u) (((double) t.tv_nsec / (double) u) + (double) (t.tv_sec * (long int) (TU_SECONDS / u)))

/* === Declarations === */

/**
 * @return the wall time, using CLOCK_MONOTONIC_RAW.
 */
timespec get_time(void);

/* === Definitions === */

benchmark* benchmark_create(size_t n)
{
	benchmark* bm;

	if ((bm = (benchmark*) malloc(sizeof(benchmark))) == NULL)
	{
		return NULL;
	}

	if ((bm->readings = (reading*) malloc(n * sizeof(reading))) == NULL)
	{
		free(bm);
		return NULL;
	}

	bm->reading_count = n;
	return bm;
}

void benchmark_time_start(reading* r, const char* name, time_unit u)
{
	timespec t = get_time();
	r->name = name;
	r->value = TTD(t, u);
}

void benchmark_time_end(reading* r, time_unit u)
{
	timespec t = get_time();
	r->value = TTD(t, u) - r->value;
}

void benchmark_free(benchmark* bm)
{
	free(bm->readings);
	free(bm);
}

reading get_max_reading(reading* r, size_t n)
{
	reading rs = r[0];

	for (size_t i = 1; i < n; i++)
	{
		if (r[i].value > rs.value)
		{
			rs = r[i];
		}
	}

	return rs;
}

timespec get_time(void)
{
	timespec t;
	if (clock_gettime(CLOCK_MONOTONIC_RAW, &t) != 0)
	{
		bail_out(EXIT_FAILURE, "clock_gettime() failed");
	}

	return t;
}
