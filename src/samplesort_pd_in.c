/*
 * samplesort_pd_in.c
 *
 *  Created on: 27 Apr 2015
 *      Author: Martin Mautner
 */

#include <omp.h>
#include "samplesort.h"

//#define USE_PARALLEL_COUNTING
//#define USE_PARALLEL_SAMPLING
//#define USE_RANDOM_SAMPLING

/* === Global variables === */

/**
 * Thread count
 */
size_t p;

/**
 * Oversampling ratio
 */
size_t s;

/* === Declarations === */

/**
 * Parses the algorithm options and invokes bail_out() on failure.
 *
 * @param[in] ds the dataset to be sorted.
 * @param[in] options the passed options.
 * @param[in] option_count the number of options passed.
 */
void parse_options(dataset* ds, char** options, size_t option_count);

/* === Implementations === */

void parse_options(dataset* ds, char** options, size_t option_count)
{
	// Parse options
	if (option_count != 2 || !str2size_t(options[0], &p) || !str2size_t(options[1], &s))
	{
		bail_out(EXIT_FAILURE, get_usage_message(), command);
	}

	// Check if 1 < p <= min(n, max_threads)
	if (p < 2 || p > ds->length || p > (size_t) omp_get_max_threads())
	{
		bail_out(EXIT_FAILURE, "1 < p <= min(n, %d) must be true", omp_get_max_threads());
	}

	// Check if s > 0
	if (s < 1)
	{
		bail_out(EXIT_FAILURE, "s must be greater than zero");
	}

	// Check if s * p <= n
	if (s * p > ds->length)
	{
		bail_out(EXIT_FAILURE, "s * p <= n must be true");
	}
}

benchmark* sort(dataset* ds, char** options, size_t option_count)
{
	// Parse options
	parse_options(ds, options, option_count);

	// Shared variables
	pivot* pvt = mem_array(pivot, p - 1);
	pivot* sample = mem_array(pivot, s * p);
	size_t* bucket_size = mem_array(size_t, p);
	size_t* bucket_offset = mem_array(size_t, p);
	size_t* bucket_next = mem_array(size_t, p);
	size_t* bucket_selected = mem_array(size_t, p);
	size_t (*count)[p] = mem_array_2d(size_t, p, p);	// count[r][i] elements will go to bucket i from thread r

	// Benchmark
	benchmark* bm;

	if ((bm = benchmark_create(4)) == NULL)
	{
		bail_out(EXIT_FAILURE, "failed to allocate memory for benchmark");
	}

	reading local_time[p];
	reading dist_time[p];

	// Sort
	#pragma omp parallel num_threads(p)
	{
			// Rank
			size_t r = (size_t) omp_get_thread_num();

			// Start timer
			benchmark_time_start(&local_time[r], "Runtime (ms)", TU_MILLISECONDS);

			// Clear buckets
			bucket_size[r] = 0;
			bucket_offset[r] = 0;
			bucket_next[r] = 0;
			bucket_selected[r] = 1;

			size_t local_count[p];

			for (size_t i = 0; i < p; i++)
			{
				local_count[i] = 0;
			}

			// Compute data segment
			size_t st, end;
			get_segment(r, ds->length, p, &st, &end);

#ifdef USE_PARALLEL_SAMPLING

			// Get sample
			for (size_t i = 0; i < s; i++)
			{
				sample[r * s + i].idx = st + get_segment_end(i, end - st, s) - 1;
				sample[r * s + i].val = ds->data[sample[r * s + i].idx];
			}

	#pragma omp barrier

			#pragma omp single
			{
#else
			#pragma omp single
			{
	#ifdef USE_RANDOM_SAMPLING

				// Get sample
				for (size_t i = 0; i < s * p - 1; i++)
				{
					// Get extents of segment
					size_t st_t, end_t;
					get_segment(i, ds->length, s * p - 1, &st_t, &end_t);

					// Take random element within segment
					sample[i].idx = st_t + ((size_t) rand() % (end_t - st_t));
					sample[i].val = ds->data[sample[i].idx];
				}
	#else

				// Get sample
				for (size_t i = 0; i < s * p - 1; i++)
				{
					sample[i].idx = get_segment_end(i, ds->length, s * p) - 1;
					sample[i].val = ds->data[sample[i].idx];
				}

	#endif
#endif

				// Sort sample
				benchmark_time_start(&bm->readings[1], "Sample sorting (ms)", TU_MILLISECONDS);

				qsort(sample, s * p - 1, sizeof(pivot), pivot_cmp);

				benchmark_time_end(&bm->readings[1], TU_MILLISECONDS);

				// Select splitters
				for (size_t i = 0; i < p - 1; i++)
				{
					pvt[i] = sample[(i + 1) * s - 1];
				}
			}

			for (size_t i = st; i < end; i++)
			{
				local_count[rank(ds->data[i], i, pvt, p - 1)]++;
			}

			for (size_t i = 0; i < p; i++)
			{
				count[r][i] = local_count[i];	// Make sure false sharing has minimal effects
			}

		#pragma omp barrier

#ifdef USE_PARALLEL_COUNTING
			for (size_t i = 0; i < p; i++)
			{
				bucket_size[r] += count[i][r];
			}

		#pragma omp barrier

			// Compute prefix sums
			#pragma omp single
			{
				prefix_sums(bucket_size, bucket_offset, 1, p);
			}
#else
			#pragma omp single
			{
				// Compute final bucket size
				for (size_t i = 0; i < p; i++)
				{
					for (size_t j = 0; j < p; j++)
					{
						bucket_size[j] += count[i][j];
					}
				}

				// Prefix sums for bucket offsets
				prefix_sums(bucket_size, bucket_offset, 1, p);
			}
#endif
			// Distribute
			benchmark_time_start(&dist_time[r], "Distribution (ms)", TU_MILLISECONDS);

			// Get last element of bucket that belongs to another bucket
			// This will be the starting element of the thread's distribution round
			elem x, xt;
			size_t b, bt;

			while (bucket_selected[r] <= bucket_size[r])
			{
				x = ds->data[bucket_offset[r] + bucket_size[r] - bucket_selected[r]];

				if ((b = rank(x,  bucket_offset[r] + bucket_size[r] - bucket_selected[r], pvt, p - 1)) != r)
				{
					break;
				}

				bucket_selected[r]++;
			}

		#pragma omp barrier

			if (bucket_selected[r] <= bucket_size[r])
			{
				for (;;)
				{
					// Find next position with unfixed element
					size_t i;

					#pragma omp atomic capture
					i = bucket_next[b]++;

					// Have we found a starting element?
					// If so, this thread's round is complete
					if (i == bucket_size[b] - bucket_selected[b])
					{
						ds->data[bucket_offset[b] + i] = x;
						break;
					}

					// Get element at position
					xt = ds->data[bucket_offset[b] + i];
					bt = rank(xt, bucket_offset[b] + i, pvt, p - 1);

					// Skip if the element is fixed already
					if (bt == b)
					{
						continue;
					}

					// Swap
					ds->data[bucket_offset[b] + i] = x;
					x = xt;
					b = bt;
				}
			}

			benchmark_time_end(&dist_time[r], TU_MILLISECONDS);

		#pragma omp barrier

			// Sort bucket
			qsort(&ds->data[bucket_offset[r]], bucket_size[r], sizeof(elem), real_cmp);

			// End time
			benchmark_time_end(&local_time[r], TU_MILLISECONDS);
	}

	// Total time
	bm->readings[0] = get_max_reading(local_time, p);

	// Distribution
	bm->readings[2] = get_max_reading(dist_time, p);

	// Bucket expansion
	bm->readings[3].name = "Bucket expansion";
	bm->readings[3].value = get_bucket_expansion(bucket_size, p, ds->length);

	// Cleanup
	mem_free();

	return bm;
}

const char* get_usage_message()
{
	return "Usage: %s [-p] [ -g | -n percent] input thread_count oversampling_ratio";
}

