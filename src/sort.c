/*
 * merge.c
 *
 *  Created on: 15 Jan 2015
 *      Author: Martin Mautner
 */

#include <time.h>
#include <limits.h>
#include "data/generator.h"
#include "data/io.h"
#include "memory.h"
#include "sort.h"

/* === Constants === */

/**
 * Number of iterations to benchmark
 */
#define ITERATIONS 100

/* === Global variables === */

/**
 * The dataset to be sorted
 */
dataset ds, tmp;

/**
 * Specifies if the dataset should be just printed.
 */
bool print_only = false;

/**
 * Specifies if either the generator or user-provided input files should be used.
 */
bool use_generator = false;

/**
 * Specifies the number of threads / cores to use.
 */
size_t proc_count = 1;

/**
 * Specifies the portion of the data to read
 */
double data_portion = 1.0;

/**
 * The path to the input file.
 */
char* input_file = NULL;

/**
 * The length of the datasets to be generated.
 */
size_t input_length = 0;

/**
 * The command name.
 */
char* command = "sort";

/* === Declarations === */

/**
 * Parses the command line arguments and calls bail_out() in case of
 * an error.
 *
 * @param argc the argument count.
 * @param argv the arguments.
 * @return the number of options for the sort algorithm.
 */
size_t parse_args(int argc, char* argv[]);

/**
 * Compares two doubles and returns the result.
 *
 * @param[in] a pointer to first number
 * @param[in] b pointer to second number
 * @return the result of the comparison
 */
int double_cmp(const void* a, const void* b);

/* === Implementations === */

int main(int argc, char* argv[])
{
	ds.data = tmp.data = NULL;

	// Seed for rand()
	srand((unsigned int) time(NULL));

	// Parse arguments
	size_t opt_count = parse_args(argc, argv);

	// Load / generate data
	if (use_generator)
	{
		ds.length = input_length;

		if (dataset_generate(&ds) == NULL)
		{
			bail_out(EXIT_FAILURE, "dataset creation failed");
		}
	}
	else
	{
		if (dataset_read(input_file, &ds, data_portion) == NULL)
		{
			bail_out(EXIT_FAILURE, "failed to read dataset");
		}
	}

	// Print only
	if (print_only)
	{
		char c = '\0';

		do
		{
			printf(C_YELLOW "dataset contains %zu elements, do want to print them? (y/n)\n" C_RESET, ds.length);
			c = (char) getchar();
		}
		while (c != 'y' && c != 'n' && c != EOF);

		if (c == 'y')
		{
			dataset_print(stdout, &ds);
			printf("\n");
		}

		free_resources();
		return EXIT_SUCCESS;
	}

#ifndef _DEBUG

	// Allocate memory for temporary dataset
	tmp.length = ds.length;

	if (dataset_create(&tmp) == NULL)
	{
		bail_out(EXIT_FAILURE, "failed to create output dataset");
	}

	// Sort
	benchmark* bm[ITERATIONS];

	for (int i = 0; i < ITERATIONS; i++)
	{
		// Copy to temporary
		dataset_copy(&tmp, &ds);

		// Sort
		bm[i] = sort(&tmp, &argv[optind], opt_count);
	}

	// Output
	for (size_t r = 0; r < bm[0]->reading_count; r++)
	{
		// Copy values in array
		double val[ITERATIONS];
		for (int i = 0; i < ITERATIONS; i++)
		{
			val[i] = bm[i]->readings[r].value;
		}

		// Sort values
		qsort(val, ITERATIONS, sizeof(double), double_cmp);

		// Get sum, min, max and mean
		double sum = .0;
		double min = bm[0]->readings[r].value;
		double max = min;
		double mean = val[ITERATIONS / 2];

		for (int i = 0; i < ITERATIONS; i++)
		{
			if (val[i] < min)
			{
				min = val[i];
			}
			else if (val[i] > max)
			{
				max = val[i];
			}

			sum += val[i];
		}

		printf("%s: " C_GREEN "min=%.2lf " C_RED "max=%.2lf " C_YELLOW "avg=%.2lf " C_CYAN "mean=%.2lf\n" C_RESET,
			   bm[0]->readings[r].name, min, max, sum / ITERATIONS, mean);
	}

	// Free
	for (int i = 0; i < ITERATIONS; i++)
	{
		benchmark_free(bm[i]);
	}

#else

	// Sort
	benchmark_free(sort(&ds, &argv[optind], opt_count));

	// Check if sorted
	size_t index;

	for (index = 1; index < ds.length; index++)
	{
		if (ds.data[index - 1] > ds.data[index])
		{
			break;
		}
	}

	if (index == ds.length)
	{
		printf(C_GREEN "dataset was sorted successfully\n" C_RESET);
	}
	else
	{
		printf(C_RED "dataset is still not sorted (%zu, %zu):\n" C_RESET, index - 1, index);
		printf(C_YELLOW "[..," ELEM_FMT ", " ELEM_FMT ",..]\n" C_RESET, ds.data[index - 1], ds.data[index]);
	}

#endif

	// Cleanup
	free_resources();

	// Return
	return EXIT_SUCCESS;
}

void free_resources(void)
{
	mem_free();
	dataset_free(&ds);
	dataset_free(&tmp);
}

size_t parse_args(int argc, char* argv[])
{
	// Get command name
	if (argc > 0)
	{
		command = argv[0];
	}

	// Parse options
	int opt;

	while ((opt = getopt(argc, argv, "pgn:")) != -1)
	{
		switch (opt)
		{
			case 'p':
				print_only = true;
				break;

			case 'g':
				use_generator = true;
				break;

			case 'n':
				if (!str2double(optarg, &data_portion) || data_portion == .0)
				{
					bail_out(EXIT_FAILURE, "n must be greater than 0");
				}
				break;

			default:
				bail_out(EXIT_FAILURE, get_usage_message(), command);
		}
	}

	// Get arguments
	if (optind > argc - 1)
	{
		bail_out(EXIT_FAILURE, get_usage_message(), command);
	}

	if (use_generator)
	{
		if (!str2size_t(argv[optind], &input_length))
		{
			bail_out(EXIT_FAILURE, get_usage_message(), command);
		}
	}
	else
	{
		input_file = argv[optind];
	}

	// Return options count
	int rs = argc - (++optind);
	return (rs > 0) ? (size_t) rs : 0;
}

int double_cmp(const void* a, const void* b)
{
	const double* x = (const double*) a;
	const double* y = (const double*) b;

	return (*x < *y) ? -1 : (*x > *y);
}
