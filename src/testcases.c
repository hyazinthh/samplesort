/*
 * testcases.c
 *
 *  Created on: 22 Jan 2015
 *      Author: Martin Mautner
 */

#include "data/io.h"
#include "data/generator.h"

/* === Declarations === */

/**
 * Struct to describe test cases
 */
typedef struct
{
	const char* name;
	bool (*generate)(dataset* ds, size_t n);

} testcase;

bool gen_random(dataset* ds, size_t n);

bool gen_uniform(dataset* ds, size_t n);

bool gen_sorted_asc(dataset* ds, size_t n);

bool gen_sorted_desc(dataset* ds, size_t n);

bool gen_repeating(dataset* ds, size_t n);

/**
 * Prints a usage message to stdout and calls exit().
 */
void print_usage();

/* === Global variables === */

testcase tc[] =
{
	{ "random", gen_random },
	{ "uniform", gen_uniform },
	{ "sorted (ascending)", gen_sorted_asc },
	{ "sorted (descending)", gen_sorted_desc },
	{ "repeating", gen_repeating }
};

const size_t NUM_TESTCASES = sizeof(tc) / sizeof(testcase);

/* === Implementations === */

void free_resources()
{
}

bool gen_random(dataset* ds, size_t n)
{
	ds->length = n;
	return dataset_generate(ds);
}

bool gen_uniform(dataset* ds, size_t n)
{
	// Generate single value
	dataset x = { NULL, 1 };
	if (!dataset_generate(&x))
	{
		return false;
	}

	elem val = x.data[0];
	dataset_free(&x);

	// Generate dataset
	ds->length = n;
	return dataset_generate_ext(ds, val, val);
}

bool gen_sorted_asc(dataset* ds, size_t n)
{
	ds->length = n;
	return dataset_generate_asc(ds);
}

bool gen_sorted_desc(dataset* ds, size_t n)
{
	ds->length = n;
	return dataset_generate_desc(ds);
}

bool gen_repeating(dataset*ds, size_t n)
{
	ds->length = n;
	return dataset_generate_repeating(ds);
}

int main(int argc, char* argv[])
{
	// Seed for rand()
	srand((unsigned int) time(NULL));

	// Command-line arguments
	if (getopt(argc, argv, "") != -1 || argc != 4)
	{
		print_usage();
	}

	size_t id, n;
	if (!str2size_t(argv[1], &id) || !str2size_t(argv[2], &n))
	{
		print_usage();
	}

	char* fn = argv[3];

	if (id >= NUM_TESTCASES)
	{
		fprintf(stdout, "len is out of range\n");
		print_usage();
	}

	// Generate
	dataset ds;
	if (!tc[id].generate(&ds, n))
	{
		bail_out(EXIT_FAILURE, "Failed to generate datasets");
	}

	// Write
	if (!dataset_write(fn, &ds))
	{
		bail_out(EXIT_FAILURE, "Failed to write datasets");
	}

	return EXIT_SUCCESS;
}

void print_usage()
{
	printf("Usage: testcases id len file\n");
	for (size_t i = 0; i < NUM_TESTCASES; i++)
	{
		printf("%zu: %s\n", i, tc[i].name);
	}

	exit(EXIT_FAILURE);
}
